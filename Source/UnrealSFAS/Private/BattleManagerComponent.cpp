// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleManagerComponent.h"

#include "UnrealSFAS/UnrealSFASCharacter.h"

// Sets default values for this component's properties
UBattleManagerComponent::UBattleManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBattleManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBattleManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UBattleManagerComponent::StartBattle(const TSubclassOf<AMonster> Monster)
{
	IsInBattle = true;
	
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	CurMonster = GetWorld()->SpawnActor<AMonster>(Monster, FVector(0.f, 0.f, 0.f), FRotator(0.f, 0.f, 0.f), SpawnParameters);
	if(ensureMsgf(CurMonster, TEXT("Monster is invalid in StartBattle")))
	{
		CurMonster->Health = CurMonster->MaxHealth;
	}
	
	AUnrealSFASCharacter* Character = Cast<AUnrealSFASCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if(ensureMsgf(Character, TEXT("Player is invalid in StartBattle")))
	{
		Character->SetCanMove(false);
		UpdatePlayerHealthOnWidget(Character->MaxHealth, Character->Health);
		UpdateEnemyHealthOnWidget(CurMonster->Health);
		SetupWidget(CurMonster->DamageBlocks);
		StartPlayerTurn();
	}
}

void UBattleManagerComponent::StartBossBattle(const TSubclassOf<AMonster> Monster)
{
	IsBossBattle = true;
	
	if(ensureMsgf(Monster, TEXT("Monster is invalid in StartBossBattle")))
	{
		StartBattle(Monster);
	}
}

void UBattleManagerComponent::EndBattle()
{
	AUnrealSFASCharacter* Character = Cast<AUnrealSFASCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if(ensureMsgf(Character, TEXT("Player is invalid in EndBattle")))
	{
		Character->SetCanMove(true);
		EndBattleBlueprint();
		CurMonster->Destroy();
		IsInBattle = false;
	
		if(IsBossBattle == true && Character->Health > 0)
		{
			Character->PlayerReachedExit();
		}
	}
}

void UBattleManagerComponent::StartPlayerTurn()
{
	if(ensureMsgf(CurMonster, TEXT("Monster is invalid in StartPlayerTurn")))
	{
		SetMonsterImage(EMonsterImage::Idle);
	}
	StartPlayerTurnBlueprint();
}

void UBattleManagerComponent::EndPlayerTurn()
{
	IsPlayerTurn = false;
	EndPlayerTurnBlueprint();
	
	AUnrealSFASCharacter* Character = Cast<AUnrealSFASCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if(ensureMsgf(Character, TEXT("Player is invalid in EndPlayerTurn")))
	{
		if(Character->Health > 0 )
		{
			FTimerHandle EndPlayerTurnTimer;
			GetWorld()->GetTimerManager().SetTimer(EndPlayerTurnTimer, [this]()
            {
                StartEnemyTurn();
            }, 1.f, false);
		}
	}
}

void UBattleManagerComponent::StartEnemyTurn()
{
	if(ensureMsgf(CurMonster, TEXT("Monster is invalid in StartEnemyTurn")))
	{
		SetMonsterImage(EMonsterImage::Idle);	
		CurMonster->PreAttackAdditionalFunctionality();
		DoEnemyAttacks();
	}
}

void UBattleManagerComponent::DoEnemyAttacks()
{
	if(ensureMsgf(CurMonster, TEXT("Monster is invalid in DoEnemyAttacks")))
	{
		SetMonsterImage(EMonsterImage::Idle);
		if(CurMonster->MonsterAttackAmount > 0)
		{
			CurMonster->MonsterAttackAmount --;
			
			//Timer for attack, need to make this recursive to run until attack amount equals zero
			FTimerHandle EnemyAttackTimer;
			if(IsValid(CurMonster))
			{
				GetWorld()->GetTimerManager().SetTimer(EnemyAttackTimer, [this]()
	         {		
	             SetMonsterImage(EMonsterImage::Attack);	
	             EnemyDoDamage(CurMonster->Strength);
	         },1.f, false);
			}
		}
		else
		{
			//Reset attack amount back to default
			CurMonster->MonsterAttackAmount = 1;
			EndEnemyTurn();
		}
	}
	
}

void UBattleManagerComponent::EnemyDoDamage(const float Damage)
{
	AUnrealSFASCharacter* Character = Cast<AUnrealSFASCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if(ensureMsgf(Character, TEXT("Character is invalid in EnemyDoDamage")))
	{
		if(Character->Health - Damage <= 0)
		{
			UpdatePlayerHealthOnWidget(Character->MaxHealth, 0.f);
			EndBattle();
			Character->PlayerDied();
		}
		else
		{
			Character->Health -= Damage;
			UpdatePlayerHealthOnWidget(Character->MaxHealth,Character->Health);

			//Recursive function
			FTimerHandle EnemyDoDamageTimer;
			GetWorld()->GetTimerManager().SetTimer(EnemyDoDamageTimer, [this]()
            {
                DoEnemyAttacks();
            },1.f, false);
		}
	}
	
}
void UBattleManagerComponent::EndEnemyTurn()
{
	if(ensureMsgf(CurMonster, TEXT("Monster is invalid in EndEnemyTurn")))
	{
		SetMonsterImage(EMonsterImage::Idle);
	
		//Delay before Player turn starts
		FTimerHandle EndEnemyTurnTimer;
		GetWorld()->GetTimerManager().SetTimer(EndEnemyTurnTimer, [this]()
	    {
			IsPlayerTurn = true;
			StartPlayerTurn();
	    }, 1.f, false);
		
	}
}

void UBattleManagerComponent::PlayerDoDamage(const float Damage)
{
	if(ensureMsgf(CurMonster, TEXT("Monster is invalid in PlayerDoDamage")))
	{
		SetMonsterImage(EMonsterImage::Hit);
		if(CurMonster->Health - Damage <= 0)
		{
			UpdateEnemyHealthOnWidget(0.f);
			
			//Pause combat slider
			EndPlayerTurnBlueprint();
			
			FTimerHandle EndBattlePlayerTimer;
			GetWorld()->GetTimerManager().SetTimer(EndBattlePlayerTimer, [this]()
            {
				EndBattle();
            }, 1.f, false);
		}
		else
		{
			CurMonster->Health -= Damage;
			UpdateEnemyHealthOnWidget(CurMonster->Health);
			EndPlayerTurn();
		}
	}
	
}

void UBattleManagerComponent::DoPlayerDamage(const int HitBlockIndex)
{
	//Check if player hit a box
	if(HitBlockIndex != -1)
	{
		const EHitType HitType = CurMonster->DamageBlocks[HitBlockIndex].HitType;
		switch (HitType)
		{
		case EHitType::Hit:
			PlayerDoDamage(1.f);			
			break;
		case EHitType::CriticalHit:	
  			PlayerDoDamage(2.f);
			break;
		case EHitType::Block:
			CurMonster->MonsterAttackAmount = 2;
			IsPlayerTurn = false;
			EndPlayerTurnBlueprint();
			StartEnemyTurn();
			break;
		default:
			UE_LOG(LogTemp, Warning, TEXT("Invalid HitType in UBattleMangerComponent::DoPlayerDamage"));
			break;
		}
		if(IsValid(CurMonster))
		{
			CurMonster->AdditionalMonsterFunctionality(HitBlockIndex);
		}
	}
	//Player Missed
	else
	{
		IsPlayerTurn = false;
		EndPlayerTurnBlueprint();
		StartEnemyTurn();
	}
	
}

bool UBattleManagerComponent::GetIsPlayerTurn()
{
	return IsPlayerTurn;
}

