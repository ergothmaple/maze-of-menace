// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealSFAS/Public/Tile.h"
#include "UnrealSFAS/UnrealSFASCharacter.h"
#include "BattleManagerComponent.h"
#include "UnrealSFAS/UnrealSFASMaze.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("StaticMesh"));
	StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	CollisionMesh = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	CollisionMesh->AttachToComponent(StaticMesh, FAttachmentTransformRules::KeepRelativeTransform);
	CollisionMesh->SetWorldScale3D(FVector(1.f, 1.f, 3.f));

	VisitedTileMaterial = CreateDefaultSubobject<UMaterial>(TEXT("VisitedTileMaterial"));
	UnVisitedTileMaterial = CreateDefaultSubobject<UMaterial>(TEXT("unVisitedTileMaterial"));

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	StaticMesh->SetStaticMesh(StaticMeshRef);

	StaticMesh->SetMaterial(0, UnVisitedTileMaterial);
	
}

void ATile::SetIsVisited() const
{
	StaticMesh->SetMaterial(0, VisitedTileMaterial);
}

void ATile::PlayerReachedExit(AUnrealSFASCharacter* PlayerCharacter)
{
	PlayerCharacter->PlayerReachedExit();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::PlayerOnTile(AUnrealSFASCharacter* PlayerCharacter)
{
	AUnrealSFASMaze* Maze = Cast<AUnrealSFASMaze>(this->GetAttachParentActor());
	if(ensureMsgf(Maze, TEXT("Maze is invalid in PlayerOnTile")))
	{
		Maze->SetTileVisibility(this);
	}
	
	switch (TileType)
	{
		case ETileType::Normal:
			break;
		case ETileType::Exit:
			StartBattle(PlayerCharacter, BossMonster, true);
			break;
		case ETileType::Monster:
			StartBattle(PlayerCharacter, Monsters[FMath::RandRange(0, Monsters.Num() - 1)], false);
			TileType = ETileType::Normal;
			break;
		case ETileType::Item:
			if(!HealIsUsed)
			{
				ShowHealWidget();
			}
			break;
		default:
			UE_LOG(LogTemp, Error, TEXT("UNDEFINED TileType in Tile::PlayerOnTile()"));
	}
}

void ATile::HealPlayer()
{
	if(HealIsUsed == false)
	{
		AUnrealSFASCharacter* Character = Cast<AUnrealSFASCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
		if(ensureMsgf(Character, TEXT("Character is invalid in HealPlayer")))
		{
			Character->HealPlayer(HealAmount);
			HealIsUsed = true;
			HideHealWidget();
		}
	}
}

void ATile::PlayerLeavingTile()
{
	switch (TileType)
	{
		case ETileType::Item:
   			HideHealWidget();
			break;
		default:
			UE_LOG(LogTemp, Warning, TEXT("Invalid TileType in ATile::PlayerLeavingTile"))
			break;
	}
}

void ATile::StartBattle(AUnrealSFASCharacter* PlayerCharacter, const TSubclassOf<AMonster> Monster,const bool IsBossBattle) const
{
	UBattleManagerComponent* BattleManager =Cast<UBattleManagerComponent>(PlayerCharacter->GetComponentByClass(UBattleManagerComponent::StaticClass()));
	
	//check if the battle is a boss or normal battle
	IsBossBattle ? BattleManager->StartBossBattle(Monster) : BattleManager->StartBattle(Monster);
}

