// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Monster.h"
#include "Components/ActorComponent.h"
#include "BattleManagerComponent.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNREALSFAS_API UBattleManagerComponent final : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBattleManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void StartBattle(TSubclassOf<AMonster> Monster);

	
	UFUNCTION()
    void StartBossBattle(TSubclassOf<AMonster> Monster);

	UFUNCTION()
	void EndBattle();
	
	UFUNCTION(BlueprintImplementableEvent)
	void EndBattleBlueprint();

	UFUNCTION(BlueprintImplementableEvent)
	void SetupWidget(const TArray<FDamageBlock>&  DamageBlocks);

	UFUNCTION()
	void StartPlayerTurn();

	UFUNCTION(BlueprintImplementableEvent)
	void StartPlayerTurnBlueprint();

	UFUNCTION()
	void EndPlayerTurn();

	UFUNCTION()
	void StartEnemyTurn();

	UFUNCTION()
	void DoEnemyAttacks();

	UFUNCTION(BlueprintImplementableEvent)
	void SetMonsterImage(EMonsterImage monsterImage);

	UFUNCTION()
	void EndEnemyTurn();

	UFUNCTION()
	void PlayerDoDamage(float Damage);

	UFUNCTION()
	void EnemyDoDamage(float Damage);
	
	UFUNCTION(BlueprintImplementableEvent)
	void EndPlayerTurnBlueprint();
	
	UFUNCTION(BlueprintCallable)
	void DoPlayerDamage(int HitBlockIndex);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePlayerHealthOnWidget(float MaxHealth, float NewHealth);

	UFUNCTION(BlueprintImplementableEvent)
    void UpdateEnemyHealthOnWidget(float NewHealth);
	
	UFUNCTION(BlueprintCallable)
    bool GetIsPlayerTurn();
	
public:
	UPROPERTY(BlueprintReadOnly)
	bool IsInBattle = false;
	
	UPROPERTY(BlueprintReadOnly)
	AMonster* CurMonster;
private:
	bool IsPlayerTurn = true;
	bool IsBossBattle = false;
		
};
