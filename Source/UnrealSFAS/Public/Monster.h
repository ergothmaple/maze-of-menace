// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Monster.generated.h"

UENUM(Blueprintable)
enum class EHitType : uint8
{
	Hit = 0 UMETA(DisplayName = "Hit"),
    CriticalHit = 1  UMETA(DisplayName = "CriticalHit"),
    Block = 2    UMETA(DisplayName = "Block"),
    Miss = 3 UMETA(DisplayName = "Miss")
};

UENUM(Blueprintable)
enum class EMonsterImage : uint8
{
	Idle = 0 UMETA(DisplayName = "Idle"),
    Hit = 1  UMETA(DisplayName = "Hit"),
    Attack = 2    UMETA(DisplayName = "Attack")
};

USTRUCT(Blueprintable)
struct FDamageBlock
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Start;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float End;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EHitType HitType;
};

UCLASS()
class UNREALSFAS_API AMonster final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMonster();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintImplementableEvent)
    void PreAttackAdditionalFunctionality();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void AdditionalMonsterFunctionality(int DamageBlockIndex);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "DamageBar")
	TArray<FDamageBlock> DamageBlocks;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "MonsterStats")
	float MaxHealth;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "MonsterStats")
	float Health;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "MonsterStats")
	float Defense;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "MonsterStats")
	float Strength;
	
	UPROPERTY(BlueprintReadWrite)
	int MonsterAttackAmount = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* IdleImage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* AttackImage;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* GetHitImage;
};
