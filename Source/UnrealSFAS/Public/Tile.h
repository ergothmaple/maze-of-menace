// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Monster.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"
class UCell;
class AUnrealSFASCharacter;
UENUM()
enum class ETileType : uint8
{
	Normal = 0 UMETA(DisplayName = "Normal"),
    Monster = 1  UMETA(DisplayName = "Monster"),
    Exit = 2    UMETA(DisplayName = "Exit"),
    Item = 3 UMETA(DisplayName = "Item")
}; 

UCLASS()
class UNREALSFAS_API ATile final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void PlayerOnTile(AUnrealSFASCharacter* PlayerCharacter);

	UFUNCTION()
	static void PlayerReachedExit(AUnrealSFASCharacter* PlayerCharacter);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowHealWidget();

	UFUNCTION(BlueprintImplementableEvent)
    void HideHealWidget();

	UFUNCTION(BlueprintCallable)
    void HealPlayer();	

	UFUNCTION()
    void SetIsVisited() const;

	UFUNCTION()
	void PlayerLeavingTile();

	UFUNCTION()
	void StartBattle(AUnrealSFASCharacter* PlayerCharacter, const TSubclassOf<AMonster> Monster, const bool IsBossBattle) const;

public:
	UPROPERTY(EditDefaultsOnly)
	class UMaterialInterface* UnVisitedTileMaterial;

	UPROPERTY(EditDefaultsOnly)
	class UMaterialInterface* VisitedTileMaterial;

	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<AMonster>> Monsters;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AMonster> BossMonster;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=StaticMeshAssets)
	UStaticMesh* StaticMeshRef;
	
	UPROPERTY()
	UStaticMeshComponent* StaticMesh = nullptr;
	
	UPROPERTY()
	UBoxComponent* CollisionMesh = nullptr;

	UPROPERTY()
	ETileType TileType = ETileType::Normal;
	
	UPROPERTY()
	bool IsExit = false;

	UPROPERTY()
	bool IsMonsterTile = false;

	UPROPERTY()
	bool HealIsUsed = false;

	UPROPERTY()
	float HealAmount = 100.f;

	UPROPERTY()
	UCell* ConnectedCell;

};
