// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealSFASCharacter.h"
#include "Tile.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"

AUnrealSFASCharacter::AUnrealSFASCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->bEditableWhenInherited = true;
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SetRelativeRotation(FRotator(-15.f, 225.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	FollowCamera->bEditableWhenInherited = true;


	//Create Movement Timeline
	MovementTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("movementTimeline"));
	TimelineFloatDelegate.BindUFunction(this, FName("TimelineFloatReturn"));
	TimelineFinished.BindUFunction(this, FName("OnTimelineFinished"));
}

void AUnrealSFASCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AUnrealSFASCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUnrealSFASCharacter::MoveRight);

}

void AUnrealSFASCharacter::SetCanMove(const bool NewCanMove)
{
	CanMove = NewCanMove;
}

bool AUnrealSFASCharacter::CanMoveInDirection(float ForwardDirection, float RightDirection) const
{
	FVector End = FVector( (MoveDistance - 100.f) * RightDirection + GetActorLocation().X, (MoveDistance - 100.f)  * ForwardDirection + GetActorLocation().Y, -10.f);

	//Ignore the player
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);
	
	FHitResult HitResult;
	//Line trace into the direction the player wants to move
	if(GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), End, ECollisionChannel::ECC_WorldDynamic, CollisionParams))
	{
		if(HitResult.bBlockingHit)
		{
			return true;
		}	
	}
	return false;
}


void AUnrealSFASCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Add Curve to the Timeline
	if(CurveFloat)
	{
		MovementTimeline->AddInterpFloat(CurveFloat, TimelineFloatDelegate, FName("Alpha"));
		MovementTimeline->SetTimelineFinishedFunc(TimelineFinished);		
	}
	
	Health = MaxHealth;

	//Raycast down and set tile visibility
	FVector End = FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z - 500.f);
	
	//Ignore the player
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);
	FHitResult HitResult;
	
	//Line trace into the direction the player wants to move
	if(GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), End, ECollisionChannel::ECC_WorldDynamic, CollisionParams))
	{
		if(HitResult.bBlockingHit)
		{
				
			ATile* Tile = Cast<ATile>(HitResult.Actor);
			if(IsValid(Tile))
			{
				Tile->PlayerOnTile(this);
			}
		}	
	}
}

void AUnrealSFASCharacter::HealPlayer(const float Amount)
{
	//make sure health doesn't go over max health;
	Health + Amount < MaxHealth ? Health += Amount : Health = MaxHealth;	
}

void AUnrealSFASCharacter::TimelineFloatReturn(const float Value)
{
	FVector NewActorLocation = CurActorLocation;
	
	if(MoveUpDown)
	{
		NewActorLocation.Y = Value * MoveDistance * MoveDirection + CurActorLocation.Y;
	}
	else
	{
		NewActorLocation.X = Value * MoveDistance * MoveDirection + CurActorLocation.X;
	}
	SetActorLocation(NewActorLocation, false);
}

void AUnrealSFASCharacter::OnTimelineFinished()
{
	
	//Allow player to move again
	CanMove = true;
	
	//Trace for tile under the player	
	//Ignore the player
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);	
	FHitResult HitResult;
	
	//Line trace into the direction the player wants to move
	if(GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), FVector( GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z - 200.f), ECollisionChannel::ECC_WorldDynamic, CollisionParams))
	{
		if(HitResult.bBlockingHit)
		{
			ATile* Tile = Cast<ATile>(HitResult.Actor);
			if(IsValid(Tile))
			{
				Tile->PlayerOnTile(this);			
			}			
		}	
	}	
}

ATile* AUnrealSFASCharacter::RaycastForTileUnder() const
{
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);	
	FHitResult HitResult;
	
	//Line trace into the direction the player wants to move
	if(GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), FVector( GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z - 200.f), ECollisionChannel::ECC_WorldDynamic, CollisionParams))
	{
		if(HitResult.bBlockingHit)
		{
			ATile* Tile = Cast<ATile>(HitResult.Actor);
			if(IsValid(Tile))
			{
				return Tile;
			}			
		}	
	}
	return nullptr;
}

void AUnrealSFASCharacter::MoveForward(const float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && CanMove == true)
	{
		MoveDirection = -Value;
		if(CanMoveInDirection(MoveDirection, 0.f))
		{
			CanMove = false;
			MoveUpDown = true;

			CurActorLocation = GetActorLocation();

			ATile* Tile = RaycastForTileUnder();
			if(IsValid(Tile))
			{
				Tile->PlayerLeavingTile();
			}
			
			MovementTimeline->PlayFromStart();		
		}		
	}
}

void AUnrealSFASCharacter::MoveRight(const float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && CanMove == true)
	{
		MoveDirection = Value;
		if(CanMoveInDirection(0.f, MoveDirection))
		{
			CanMove = false;
			MoveUpDown = false;
			
			CurActorLocation = GetActorLocation();
			ATile* Tile = RaycastForTileUnder();
			if(IsValid(Tile))
			{
				Tile->PlayerLeavingTile();
			}
			MovementTimeline->PlayFromStart();		
		}		
	}
}