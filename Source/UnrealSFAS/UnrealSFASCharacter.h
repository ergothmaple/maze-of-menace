// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Tile.h"
#include "GameFramework/Character.h"
#include "Components/TimelineComponent.h"

#include "UnrealSFASCharacter.generated.h"



class UCurveFloat;

UCLASS(config=Game)
class AUnrealSFASCharacter final : public ACharacter
{
	GENERATED_BODY()

public:
	AUnrealSFASCharacter();

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void PlayerReachedExit();

	UFUNCTION(BlueprintImplementableEvent)
	void PlayerDied();

	UFUNCTION()
	void HealPlayer(float Amount);

	UFUNCTION()
	void TimelineFloatReturn(float Value);
	
	UFUNCTION()
	void OnTimelineFinished();

	UFUNCTION(BlueprintCallable)
	ATile* RaycastForTileUnder() const;

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION(BlueprintCallable)
    void SetCanMove(bool NewCanMove);

public:
	UPROPERTY()
	FOnTimelineFloat TimelineFloatDelegate;

	UPROPERTY()
	FOnTimelineEvent TimelineFinished;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, Category = "BattleManager")
	class UBattleManagerComponent* BattleManagerComponent;
	
	UPROPERTY(BlueprintReadOnly)
	float MaxHealth = 500;
	
	UPROPERTY(BlueprintReadOnly)
	float Health;

protected:
	void MoveForward(float Value);

	void MoveRight(float Value);

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:	
	UPROPERTY()
	FTimeline CurveFTimeline;

	UPROPERTY(EditAnywhere, Category="MovementTimeline")
	UCurveFloat* CurveFloat;

	UPROPERTY()
	UTimelineComponent* MovementTimeline;

private:
	bool CanMoveInDirection(float ForwardDirection, float RightDirection) const;
	
	bool CanMove = true;
	
	float MoveDistance = 300.f;
	
	float MoveDirection = 0.f;
	
	FVector CurActorLocation;
	
	//True if player wants to move up or down, false if player wants to move left/right
	bool MoveUpDown;

};

