// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealSFASMaze.h"
#include "UnrealSFASCharacter.h"

// Sets default values
AUnrealSFASMaze::AUnrealSFASMaze()
{
 	// No need to tick the maze at this point
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AUnrealSFASMaze::BeginPlay()
{
	Super::BeginPlay();

	GenerateMaze();
	BuildMaze();

	AUnrealSFASCharacter* Character = Cast<AUnrealSFASCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if(ensureMsgf(Character, TEXT("Invalid Character in AUnrealSFASMaze::BeginPlay()")))
	{
		const float YPos = (MazeCells.Num() / 2) * DistanceBetweenTiles;
		const float XPos = (MazeCells[0]->RowCells.Num() / 2) * DistanceBetweenTiles;
		const FVector WorldLoc = FVector(XPos , YPos , 300.f);
		Character->SetActorLocation(WorldLoc);
	}
	
	
}
void AUnrealSFASMaze::BuildMaze()
{
	//Loop through Cells and create their tiles
	for(int Y = 0; Y < MazeCells.Num(); Y++)
	{
		for(int X = 0; X < MazeCells[Y]->RowCells.Num(); X++)
		{
			float YPos = Y * DistanceBetweenTiles;
			float XPos = X * DistanceBetweenTiles;
			float BlockZPos = 50.f;
			FVector WorldPosition(XPos, YPos, BlockZPos);
			FRotator WorldRotation = FRotator(0.f, 0.f, 0.f);
			FVector GroundWorldScale(1.5f, 1.5f, 1.f);
			FTransform WorldXForm(WorldRotation, WorldPosition, GroundWorldScale);
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.Owner = this;
			ATile* Tile = GetWorld()->SpawnActor<ATile>(TileToSpawn, WorldXForm,SpawnParameters);
			if(ensureMsgf(Tile, TEXT("Invalid Tile in BuildMaze")))
			{
				Tile->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
				MazeCells[Y]->RowCells[X]->ConnectedTile = Tile;
				Tile->ConnectedCell = MazeCells[Y]->RowCells[X];
				Tile->StaticMesh->SetVisibility(false);
				Tiles.Add(Tile);

				MazeCells[Y]->RowCells[X]->ConnectedCells = MazeCells[Y]->RowCells[X]->ConnectedNeighbours;
			}
		}
	}
	
	USceneComponent* ObjectRootComponent = GetRootComponent();
	
	//Loop through cells and create links
	for(int Y = 0; Y < MazeCells.Num(); Y++)
	{
		for(int X = 0; X < MazeCells[Y]->RowCells.Num(); X++)
		{
			float YPos = Y * DistanceBetweenTiles;
			float XPos = X * DistanceBetweenTiles;
			for(UCell* Cell : MazeCells[Y]->RowCells[X]->ConnectedNeighbours)
			{
				if(IsValid(Cell))
				{
					float NewYPos = Cell->Y * DistanceBetweenTiles;
					float NewXPos = Cell->X * DistanceBetweenTiles;

					float YDiff = (YPos - NewYPos) / 2;
					float XDiff = (XPos - NewXPos) / 2;
					float BlockZPos = 50.f;
					FVector WorldPosition(XPos - XDiff, YPos - YDiff, BlockZPos);
					FRotator WorldRotation = FRotator(0.f, 0.f, 0.f);
					FVector GroundWorldScale((XDiff == 0 ? 0.7f : 2.2f),(YDiff == 0 ? 0.7f : 2.2f), 0.5f);
					FTransform WorldXForm(WorldRotation, WorldPosition, GroundWorldScale);
					UStaticMeshComponent* MeshComponent = NewObject<UStaticMeshComponent>(this);
					if(ensureMsgf(MeshComponent, TEXT("MeshComponent invalid in Buildmaze link building")))
					{
						MeshComponent->SetStaticMesh(WallMesh);
						MeshComponent->SetWorldTransform(WorldXForm);
						MeshComponent->AttachToComponent(ObjectRootComponent, FAttachmentTransformRules::KeepWorldTransform);
						MeshComponent->RegisterComponent();
						MeshComponent->SetVisibility(false);

						Cell->Connections.Add(MeshComponent);
						MazeCells[Y]->RowCells[X]->Connections.Add(MeshComponent);
					}
				
					//Directly deleting the cell doesn't work for some reason, so we need to get the index and remove the cell by index
					int ItemIndex = Cell->ConnectedNeighbours.Find(MazeCells[Y]->RowCells[X]);
					if(ItemIndex != INDEX_NONE)
					{
						Cell->ConnectedNeighbours.RemoveAt(ItemIndex);
					}
				}
			}
			//Remove after for loop so it doesnt break
			MazeCells[Y]->RowCells[X]->ConnectedNeighbours.Empty();
		}
	}

	//Add exit to the maze
	UCell* ExitCell = nullptr;
	//While none found, attempt to set one
	while (ExitCell == nullptr)
	{
		int X = FMath::RandRange(0, MazeCells[0]->RowCells.Num() - 1);
		int Y = FMath::RandRange(0, MazeCells.Num() - 1);

		//Check if the exit is not on player's starting position (Player always starts in the middle of the maze)
		if(X != (MazeCells[0]->RowCells.Num() / 2) && Y != (MazeCells.Num() / 2))
		{
			ExitCell = MazeCells[0]->RowCells[0];
			ExitCell->ConnectedTile->TileType = ETileType::Exit;
		}	
	}

	//Add monsters to the maze
	while(MonsterCount > 0)
	{
		int X = FMath::RandRange(0, MazeCells[0]->RowCells.Num() - 1);
		int Y = FMath::RandRange(0, MazeCells.Num() - 1);

		//Check if the exit is not on player's starting position (Player always starts in the middle of the maze)
		if(X != (MazeCells[0]->RowCells.Num() / 2) && Y != (MazeCells.Num() / 2))
		{
			//Check if tile has no monster or exit
			if(MazeCells[Y]->RowCells[X]->ConnectedTile->TileType == ETileType::Normal)
			{
				MazeCells[Y]->RowCells[X]->ConnectedTile->TileType = ETileType::Monster;
				MonsterCount --;
			}
		}
	}

	//Add Heal spots to the maze
	while(HealCount > 0)
	{
		int X = FMath::RandRange(0, MazeCells[0]->RowCells.Num() - 1);
		int Y = FMath::RandRange(0, MazeCells.Num() - 1);

		//Check if the exit is not on player's starting position (Player always starts in the middle of the maze)
		if(X != (MazeCells[0]->RowCells.Num() / 2) && Y != (MazeCells.Num() / 2))
		{
			//Check if tile has no monster or exit
			if(MazeCells[Y]->RowCells[X]->ConnectedTile->TileType == ETileType::Normal)
			{
				MazeCells[Y]->RowCells[X]->ConnectedTile->TileType = ETileType::Item;
				HealCount --;
			}
		}
	}	
}
void AUnrealSFASMaze::GenerateMaze()
{
	//Array is ordered by Height<Width>
	
	//Create Maze cells
	for(int i = 0; i < Height; i++)
	{
		URow* Row = NewObject<URow>();
		for(int j = 0; j < Width; j++)
		{
			UCell* Cell;
			Cell = NewObject<UCell>();
			if(IsValid(Cell))
			{
				Cell->X = j;
				Cell->Y = i;
				Row->RowCells.Add(Cell);
			}
		}
		if(IsValid(Row))
		{
			MazeCells.Add(Row);
		}
	}
			
	//Mark starting cell and set as visited
	AddUnvisitedNeighboursToFrontier(0, 0);
	MazeCells[0]->RowCells[0]->IsVisited = true;
	
	//iterate until frontier is empty
	while (Frontier.Num() != 0)
	{
		//Choose random frontier cell
		const int RandCellNum = FMath::RandRange(0, Frontier.Num() - 1);
		//Get visited neighbours from frontier cell
		TArray<UCell*> VisitedNeighbours = GetVisitedNeighbours(Frontier[RandCellNum]->X, Frontier[RandCellNum]->Y);
		if(VisitedNeighbours.Num() > 0)
		{
			//choose random visited neighbour
			const int RandVisitedCellNum = FMath::RandRange(0, VisitedNeighbours.Num() - 1);
			UCell* VisitedCell = VisitedNeighbours[RandVisitedCellNum];
			if(IsValid(VisitedCell))
			{
				//link them
				Frontier[RandCellNum]->ConnectedNeighbours.Add(VisitedCell);
				VisitedCell->ConnectedNeighbours.Add(Frontier[RandCellNum]);

				//Set cell as IsVisited and mark it's neighbours as frontier and remove from frontier
				Frontier[RandCellNum]->IsVisited = true;
				UCell* Cell = Frontier[RandCellNum];
				Frontier.Remove(Cell);
				AddUnvisitedNeighboursToFrontier(Cell->X, Cell->Y);
			}
		}
	}
}

void AUnrealSFASMaze::AddUnvisitedNeighboursToFrontier(int X, int Y)
{
	//up
	if(Y - 1 >= 0)
	{
		if(MazeCells[Y-1]->RowCells[X]->IsVisited == false)
		{
			UCell* AddedCell = MazeCells[Y-1]->RowCells[X];
			Frontier.Add(AddedCell);
		}
	}
	//down
	if(Y + 1 <= MazeCells.Num() - 1)
	{
		if(MazeCells[Y+1]->RowCells[X]->IsVisited == false)
		{
			UCell* AddedCell = MazeCells[Y+1]->RowCells[X];
			Frontier.Add(AddedCell);
		}
	}
	//left
	if(X - 1 >= 0)
	{
		if(MazeCells[Y]->RowCells[X-1]->IsVisited == false)
		{
			UCell* AddedCell = MazeCells[Y]->RowCells[X - 1];
			Frontier.Add(AddedCell);
		}
	}
	//right
	if(X + 1 <= MazeCells[Y]->RowCells.Num() - 1)
	{
		if(MazeCells[Y]->RowCells[X+1]->IsVisited == false)
		{
			UCell* AddedCell = MazeCells[Y]->RowCells[X + 1];
			Frontier.Add(AddedCell);
		}
	}
}

void AUnrealSFASMaze::SetTileVisibility(ATile* Tile)
{
	//Set tiles from previous turn back to hidden
	for(UStaticMeshComponent* Mesh : VisibleMazeParts)
	{
		if(IsValid(Mesh))
		{
			Mesh->SetVisibility(false);
		}
	}

	VisibleMazeParts.Empty();
		
	if(IsValid(Tile->ConnectedCell))
	{
		//Set connections to visible
		for (UStaticMeshComponent* Connection : Tile->ConnectedCell->Connections)
		{
			if(IsValid(Connection))
			{
				Connection->SetVisibility(true);
				VisibleMazeParts.Add(Connection);
			}
		}
		//set neighbouring tiles to visible
		for(UCell* Cell : Tile->ConnectedCell->ConnectedCells)
		{
			if(IsValid(Cell))
			{
				Cell->ConnectedTile->StaticMesh->SetVisibility(true);
				VisibleMazeParts.Add(Cell->ConnectedTile->StaticMesh);
			}
		}
	}
	Tile->SetIsVisited();
	//set own tile to visible
	Tile->StaticMesh->SetVisibility(true);
	VisibleMazeParts.Add(Tile->StaticMesh);
}

TArray<UCell*> AUnrealSFASMaze::GetVisitedNeighbours(const int X, const int Y)
{
	TArray<UCell*> VisitedCells;
	//up
	if(Y - 1 >= 0)
	{
		if(MazeCells[Y-1]->RowCells[X]->IsVisited)
		{
			VisitedCells.Add(MazeCells[Y-1]->RowCells[X]);
		}
	}
	//down
	if(Y + 1 <= MazeCells.Num() - 1)
	{
		if(MazeCells[Y+1]->RowCells[X]->IsVisited)
		{
			VisitedCells.Add(MazeCells[Y+1]->RowCells[X]);
		}
	}
	//left
	if(X - 1 >= 0)
	{
		if(MazeCells[Y]->RowCells[X - 1]->IsVisited)
		{
			VisitedCells.Add(MazeCells[Y]->RowCells[X - 1]);
		}
	}
	//right
	if(X + 1 <= MazeCells[0]->RowCells.Num() - 1)
	{
		if(MazeCells[Y]->RowCells[X + 1]->IsVisited)
		{
			VisitedCells.Add(MazeCells[Y]->RowCells[X + 1]);
		}		
	}
	return VisitedCells;
}
