// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UnrealSFAS/Public/Tile.h"
#include "UnrealSFASMaze.generated.h"


struct FCell
{
	float X;
	float Y;	
	UPROPERTY()
	TArray<FCell*> ConnectedNeighbours;
	bool IsVisited = false;
};

UCLASS()
class UCell final : public UObject
{
	GENERATED_BODY()
public:
	float X;
	float Y;
	UPROPERTY()
	TArray<UCell*> ConnectedNeighbours;
	bool IsVisited = false;
	UPROPERTY()
	TArray<UStaticMeshComponent*> Connections;	
	UPROPERTY()
	TArray<UCell*> ConnectedCells;	
	UPROPERTY()
	ATile* ConnectedTile;
};

//Need to create rows for the 2D Array like this, cause unreal doesn't allow 2d arrays of UObjects
UCLASS()
class URow final : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY()
	TArray<UCell*> RowCells;
};


UCLASS()
class UNREALSFAS_API AUnrealSFASMaze final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUnrealSFASMaze();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	void SetTileVisibility(ATile* Tile);

	TArray<UCell*> GetVisitedNeighbours(int X, int Y);
	
	UPROPERTY(EditDefaultsOnly, Category = Maze)
	UStaticMesh* WallMesh;

	UPROPERTY(BlueprintReadOnly, Category = Maze)
	TArray<ATile*> Tiles;

	UPROPERTY(EditDefaultsOnly, Category = Maze)
	TSubclassOf<ATile> TileToSpawn;
	
	UPROPERTY()
	TArray<URow*> MazeCells;

	UPROPERTY()
	TArray<UCell*> Frontier;

	UPROPERTY()
	TArray<UStaticMeshComponent*> VisibleMazeParts;
	
private:
	void GenerateMaze();
	void BuildMaze();
	void AddUnvisitedNeighboursToFrontier(int X, int Y);
		
	int MonsterCount = 25;
	int HealCount = 15;	
	int Width = 10;
	int Height = 10;
	float DistanceBetweenTiles = 300.f;
};
